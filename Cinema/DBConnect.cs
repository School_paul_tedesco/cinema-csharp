﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class DBConnect : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Salle> Salles { get; set; }
        public DbSet<Movie> Movie { get; set; }
    }
}
