﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class User
    {
        public int UserId { get; private set; }
        public string Username { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public string Role { get; private set; }

        public override string ToString()
        {
            return "User : " + this.Username + "\nEmail : " + this.Email + "\nRole : " + this.Role;
        }
        public static User Login()
        {
            Console.Write("Email: ");
            string mail = Console.ReadLine();
            Console.Write("Password: ");
            string password = Console.ReadLine();
            DBConnect DB = new DBConnect();
            var userResult = (from u in DB.User where u.Email == mail && u.Password == password select u);
            if (!userResult.Any())
            {
                return new User() { Username = "-1" }; ;
            }
            else
            {
                User user = userResult.First();
                return user;
            }
        }
        public static User Register()
        {
            DBConnect DB = new DBConnect();
            Console.Write("Username: ");
            string username = Console.ReadLine();
            Console.Write("Email: ");
            string email = Console.ReadLine();
            if (!IsValidEmail(email))
            {
                return new User() { Username = "-1" };
            }
            Console.Write("Password: ");
            string password = Console.ReadLine();
            var userResult = (from uCheck in DB.User select uCheck);
            string role = (!userResult.Any()) ? "Admin" : "User";
            User u = new User()
            {
                Username = username,
                Email = email,
                Password = password,
                Role = role
        };
            if (u != null)
            {
                DB.User.Add(u);
                DB.SaveChanges();

                return u;
            }
            else return new User() { Username = "0" }; ;
        }
public static bool IsValidEmail(string email)
{
    try
    {
        var addr = new System.Net.Mail.MailAddress(email);
        return addr.Address == email;
    }
    catch
    {
        return false;
    }
}
    }
}
