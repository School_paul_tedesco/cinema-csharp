﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cinema
{
    class Menu
    {
        private int MenuChoice = 0;
        public int MenuDef()
        {
            Console.WriteLine("Menu:");
            Console.WriteLine("1 - Login");
            Console.WriteLine("2 - Register");
            MenuChoice = checkMenu(2);
            return MenuChoice;
        }
        public int MenuAdmin()
        {
            Console.WriteLine("Admin Menu :");
            Console.WriteLine("1 - Salle\n2 - Movie\n3 - Exit");
            MenuChoice = checkMenu(3);
            return MenuChoice;
        }
        public int MenuSalle()
        {
            Console.WriteLine("Salle Menu :");
            Console.WriteLine("1 - Create Salle\n2 - Add Film\n3 - Exit");
            MenuChoice = checkMenu(3);
            return MenuChoice;
        }
        public int MenuMovie()
        {
            Console.WriteLine("Movie Menu :");
            Console.WriteLine("1 - Create Movie\n2 - Add Hour\n3 - Exit");
            MenuChoice = checkMenu(3);
            return MenuChoice;
        }
        public int MenuUser()
        {
            Console.WriteLine("User Menu :");
            Console.WriteLine("1 - New ticket\n2 - Recherche Movie\n3 - Exit");
            MenuChoice = checkMenu(3);
            return MenuChoice;
        }
        //On check si il s'agit bien d'un nombre // et qu'il est dans les choix
        public static int checkMenu(int nbrChoice)
        {
            string input = Console.ReadLine();
            Console.Clear();
            //Retour un resultat
            if (!int.TryParse(input, out int result))
            {
                return 0;
            }
            for (int i = 1; i < nbrChoice + 1; i++)
            {
                if (result == (i))
                {
                    return result;
                }
            }
            return -1;
        }
    }
}
